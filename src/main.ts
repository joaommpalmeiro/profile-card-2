import OnuUI from 'onu-ui'
import { createApp } from 'vue'

import App from './App.vue'

// https://github.com/unocss/unocss/tree/main/packages/reset
import '@unocss/reset/tailwind.css'

// https://onu.zyob.top/guide/install.html#full-import
// Note: Import the styles in the following order to ensure that styles
// applied with Tailwind (instead of Onu UI) work as expected in production:
// import 'onu-ui/dist/style.css'
// import 'uno.css'

// Note: The previous approach splits the Message component.
// Keep the original order and use !important flags:
import 'uno.css'
import 'onu-ui/dist/style.css'

createApp(App).use(OnuUI).mount('#app')
