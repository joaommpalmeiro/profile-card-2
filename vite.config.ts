import vue from '@vitejs/plugin-vue'
import { presetOnu } from 'onu-ui'
import { presetIcons, presetUno, presetWebFonts } from 'unocss'
import UnoCSS from 'unocss/vite'
import { defineConfig } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    // https://github.com/unocss/unocss/tree/main/packages/vite
    // https://github.com/onu-ui/onu-ui/blob/main/playground/vite.config.ts
    // https://github.com/unocss/unocss
    // https://github.com/onu-ui/onu-ui/blob/main/docs/vite.config.ts
    // https://github.com/onu-ui/onu-ui/blob/main/docs/unocss.config.ts
    // https://github.com/unocss/unocss/blob/main/examples/vite-vue3/vite.config.ts
    // https://github.com/unocss/unocss#using-presets
    // https://github.com/onu-ui/onu-ui/blob/main/playground/unocss.config.ts
    // https://github.com/onu-ui/onu-ui/blob/main/example/unocss.config.ts
    // https://github.com/vuejs/vitepress/blob/main/src/client/theme-default/styles/fonts.css
    // https://github.com/unocss/unocss/tree/main/packages/unocss + https://github.com/unocss/unocss/blob/main/packages/unocss/package.json
    UnoCSS({
      presets: [
        // https://github.com/onu-ui/onu-ui/blob/main/example/unocss.config.ts#L6
        // https://github.com/unocss/unocss/blob/main/packages/preset-uno/src/index.ts#L28
        // https://github.com/unocss/unocss/blob/main/packages/preset-mini/src/index.ts#L95
        // preflight is relevant if variablePrefix is different from the default ('un-'):
        presetUno({ preflight: false }),
        // presetUno({
        //   attributifyPseudo: true,
        // }),
        // presetAttributify(),
        presetWebFonts({
          provider: 'google',
          fonts: {
            // https://github.com/unocss/unocss/blob/main/packages/preset-web-fonts/src/providers/google.ts
            // https://fonts.google.com/specimen/Inter
            sans: 'Inter:100..900',
            mono: 'Space Mono',
          },
        }),
        presetIcons({
          cdn: 'https://esm.sh/',
        }),
        // https://github.com/onu-ui/onu-ui/tree/main/packages/preset
        presetOnu(),
      ],
      // transformers: [transformerVariantGroup(), transformerDirectives()],
    }),
  ],
  server: {
    open: true,
    // For WSL/Windows:
    // https://youtu.be/BUClW9wTqGQ
    // https://vitejs.dev/config/server-options.html#server-watch
    watch: {
      usePolling: true,
    },
  },
})
