# profile-card-2

> https://joaommpalmeiro.gitlab.io/profile-card-2

An example of a profile card created with Vue, Vite, UnoCSS, and Onu UI.

## Screenshots

![Full page](assets/Screenshot%202023-01-26%20at%2023.57.17.png)

### Toggle the squares to see my previous job

![Toggle the squares to see my previous job](assets/Screenshot%202023-01-26%20at%2023.57.22.png)

### Copy the email address to the clipboard

![Copy the email address to the clipboard](assets/Screenshot%202023-01-26%20at%2023.57.25.png)

### Button style when hovered

![Button style when hovered](assets/Screenshot%202023-01-26%20at%2023.57.36.png)

## Development

Enable [Volar's Takeover Mode](https://vuejs.org/guide/typescript/overview.html#volar-takeover-mode).

```bash
npm install
```

```bash
npm run dev
```

## References

- https://github.com/vitejs/vite/tree/main/packages/create-vite/template-vue-ts
- https://github.com/antfu/vitesse-lite
- https://github.com/antfu/eslint-config + https://github.com/antfu/eslint-config/blob/main/packages/all/package.json
- https://github.com/vuejs/create-vue
- https://emojicon.dev/

## Notes

- https://stackoverflow.com/questions/72027949/why-does-vite-create-two-typescript-config-files-tsconfig-json-and-tsconfig-nod: "You need two different TS configs because the project is using two different environments [browser and Node.js] in which the TypeScript code is executed (...)"
- `npm create vite@latest profile-card-2 -- --template vue-ts`
- https://vuejs.org/guide/typescript/overview.html#volar-takeover-mode + https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin
- `npm install vue && npm install -D @vitejs/plugin-vue typescript vite vue-tsc eslint @antfu/eslint-config`
- https://github.com/eslint/eslint-plugin-markdown: "Lint JS, JSX, TypeScript, and more inside Markdown."
- https://github.com/remarkjs/vscode-remark
- https://github.com/BenoitZugmeyer/eslint-plugin-html: "This plugin focuses on applying ESLint rules on inline scripts contained in HTML. It does not provide any rule related to HTML."
- https://yeonjuan.github.io/html-eslint/: `npm install -D @html-eslint/parser @html-eslint/eslint-plugin`
- `npm install onu-ui && npm install -D unocss`
- `npm install -D @unocss/preset-attributify`
- https://www.hyperui.dev/
